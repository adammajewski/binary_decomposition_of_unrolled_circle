/*

  Adam Majewski
  adammaj1 aaattt o2 dot pl  // o like oxygen not 0 like zero 
  fraktal.republika.pl

  c console progam 


  How to compute iteration : 

  gcc r.c -lm -Wall -march=native 
  time ./a.out

  m
*/

#include <stdio.h>
#include <stdlib.h> // malloc
#include <string.h> // strcat
#include <math.h> // M_PI; needs -lm also 
#include <complex.h>

/* --------------------------------- global variables and consts ------------------------------------------------------------ */





// virtual 2D array and integer ( screen) coordinate
// Indexes of array starts from 0 not 1 
//unsigned int ix, iy; // var
static unsigned int ixMin = 0; // Indexes of array starts from 0 not 1
static unsigned int ixMax ; //
static unsigned int iWidth ; // horizontal dimension of array

static unsigned int iyMin = 0; // Indexes of array starts from 0 not 1
static unsigned int iyMax ; //

static unsigned int iHeight = 2000; //  
// The size of array has to be a positive constant integer 
static unsigned int iSize ; // = iWidth*iHeight; 

// memmory 1D array 

unsigned char *data;
unsigned char *edge;
unsigned char *edge1;

// unsigned int i; // var = index of 1D array
//static unsigned int iMin = 0; // Indexes of array starts from 0 not 1
static unsigned int iMax ; // = i2Dsize-1  = 
// The size of array has to be a positive constant integer 
// unsigned int i1Dsize ; // = i2Dsize  = (iMax -iMin + 1) =  ;  1D array with the same size as 2D array

/* world ( double) coordinate = dynamic plane */
static   const double ZxMin=-10.0;
static  const double ZxMax=10.0;
static  const double ZyMin=-10.0;
static  const double ZyMax=10.0;
static  double PixelWidth; // =(ZxMax-ZxMin)/ixMax;
static  double PixelHeight; // =(ZyMax-ZyMin)/iyMax;
static  double ratio ;
 




static unsigned long int iterMax  = 1000; //iHeight*100;

static double ER = 9.0; // Escape Radius for bailout test 
static double ER2;

/* colors = shades of gray from 0 to 255 */
// 8 bit color = int number from 0 to 255
unsigned char iColorOfInterior=200; // 
static unsigned char iColorOfExteriorUp = 125;
static unsigned char iColorOfExteriorDown = 245;

static unsigned char iColorOfUnknown = 100;

long int iUknownPixels=0;


const double pi = 3.141592653589793;
/* ------------------------------------------ functions -------------------------------------------------------------*/



      

//------------------complex numbers -----------------------------------------------------



// from screen to world coordinate ; linear mapping
// uses global cons
complex double GiveZ(unsigned int ix, unsigned int iy)
{ 
  double Zx, Zy; // Z=Zx+Zy*I = radius*e^{turn*2*pi*I}
  double turn = ((double) ix)/((double ) ixMax); // arg(Z)
  double radius = ((double) (iyMax-iy))/((double) iyMax);  // abs(Z)
  Zx = ZxMax*radius*cos(turn*2.0*pi);
  Zy = ZyMax*radius*sin(turn*2.0*pi); 
 

  return (Zx+ I*Zy);

}

// uses globaal cons
//double GiveZy(unsigned int ix, unsigned int iy)
// { return (ZyMax - iy*PixelHeight);} // reverse y axis

/* -----------  array functions = drawing -------------- */

/* gives position of 2D point (ix,iy) in 1D array  ; uses also global variable iWidth */
unsigned int Give_i(unsigned int ix, unsigned int iy)
{ return ix + iy*iWidth; }

// plots raster point (ix,iy) 
int iDrawPoint(unsigned char A[], unsigned int ix, unsigned int iy, unsigned char iColor)
{ 

  /* i =  Give_i(ix,iy) compute index of 1D array from indices of 2D array */
  A[Give_i(ix,iy)] = iColor;

  return 0;
}


 





//;;;;;;;;;;;;;;;;;;;;;;  setup ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

int setup()
{

  
 
  
  
  printf("setup\n");
 
  /* 2D array ranges */
  
  iWidth = iHeight;
  iSize = iWidth*iHeight; // size = number of points in array 
  // iy
  iyMax = iHeight - 1 ; // Indexes of array starts from 0 not 1 so the highest elements of an array is = array_name[size-1].
  //ix
  
  ixMax = iWidth - 1;

  /* 1D array ranges */
  // i1Dsize = i2Dsize; // 1D array with the same size as 2D array
  iMax = iSize-1; // Indexes of array starts from 0 not 1 so the highest elements of an array is = array_name[size-1].

  /* Pixel sizes */
  PixelWidth = (ZxMax-ZxMin)/ixMax; //  ixMax = (iWidth-1)  step between pixels in world coordinate 
  PixelHeight = (ZyMax-ZyMin)/iyMax;
  ratio = ((ZxMax-ZxMin)/(ZyMax-ZyMin))/((float)iWidth/(float)iHeight); // it should be 1.000 ...
  
  

  // for numerical optimisation in iteration
  ER2 = ER * ER;
  
  
  /* create dynamic 1D arrays for colors ( shades of gray ) */
  data = malloc( iSize * sizeof(unsigned char) );
  edge = malloc( iSize * sizeof(unsigned char) );
  edge1 = malloc( iSize * sizeof(unsigned char) );

  if (edge1==NULL || edge== NULL || data == NULL)
    {
      fprintf(stderr," Could not allocate memory");
      getchar(); 
      return 1;
    }

  

 
  

  
   
  
  printf(" end of setup \n");
  
  return 0;

} // ;;;;;;;;;;;;;;;;;;;;;;;;; end of the setup ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

unsigned char ComputeColor(unsigned int ix, unsigned int iy, int IterationMax, int iMethod)
{ 
  // check behavour of z under fc(z)=z^2+c
  // using 2 target set:
  // 1. exterior or circle (center at origin and radius ER ) 
  // as a target set containing infinity = for escaping points ( bailout test)
  // for points of exterior of julia set
  // 2. interior of circle with center = alfa and radius dMaxDistance2fixed
  // as a target set for points of interior of Julia set 
  //  Z= Zx+ZY*i;

 
  int i=0; // number of iteration 
  
   
  complex double Z; // Z= Zx + Zy*I
  double Zx, Zy; 
  double Zx2, Zy2; // Zx2 = Zx* Zx
  
  
  // from screen to world coordinate 
  Z = GiveZ(ix,iy);
  Zx = creal(Z);
  Zy = cimag(Z);
  //
  Zx2=Zx*Zx;
  Zy2=Zy*Zy;
  
  
  

  if (Zx2+Zy2<1.0) return  iColorOfInterior;
  if (Zx2+Zy2>ER2)  
    {  switch( iMethod )
	{
            
    
	case 1: // level set method
	  if (i%2 == 0) return 150; 
	  else return 190;
	  break;

	case 2: // binary decomposition method 
	  if (Zy>0) return iColorOfExteriorUp; 
	  else return iColorOfExteriorDown;
	  break;


	} // switch 
           
    }
  	
   
    
  // if not inside target set around 
  while (i< IterationMax)
    { // then iterate 
      Zy=2*Zx*Zy ;      
      Zx=Zx2-Zy2 ;
      Zx2=Zx*Zx;
      Zy2=Zy*Zy;
            
      // escaping test
      if (Zx2+Zy2>ER2)  
	{  switch( iMethod )
	    {
            case 1: // level set method
              if (i%2 == 0) return 100; 
	      else return 200;
	      break;

            case 2: // binary decomposition method 
	      if (Zy>0) return iColorOfExteriorUp; 
	      else return iColorOfExteriorDown;
	      break;

	    } // switch 
           
	}
      // if escaping stop iteration
      i+=1;
    }

  // pixel is not escaping to infinity or not attracting to fixed attractore : 
  // change parameters : iterMax, distance ...
  iUknownPixels+=1;
  return  iColorOfUnknown ;   //
}

// plots raster point (ix,iy) 
int PlotPoint(unsigned char A[] , unsigned int ix, unsigned int iy, int IterationMax, int iMethod)
{
  unsigned i; /* index of 1D array */
  unsigned char iColor;
  

  i = Give_i(ix,iy); /* compute index of 1D array from indices of 2D array */
  iColor = ComputeColor(ix, iy, IterationMax, iMethod);
  A[i] = iColor;

  return 0;
}

// fill array 
// uses global var :  ...
// scanning complex plane 
int FillArray(unsigned char A[], int IterationMax, int iMethod )
{
  unsigned int ix, iy; // pixel coordinate 

  printf("compute image \n");
  // for all pixels of image 
  for(iy = iyMin; iy<=iyMax; ++iy) 
    { printf(" %d z %d\n", iy, iyMax); //info 
      for(ix= ixMin; ix<=ixMax; ++ix) PlotPoint(A, ix, iy, IterationMax, iMethod ) ; //  
    } 
   
  return 0;
}

int ComputeBoundariesFromA2B(unsigned char A[], unsigned char B[])
{
 
  unsigned int iX,iY; /* indices of 2D virtual array (image) = integer coordinate */
  unsigned int i; /* index of 1D array  */
  /* sobel filter */
  unsigned char G, Gh, Gv; 
  // boundaries are in edge array ( global var )
 
 
 
 
  printf(" find boundaries in A array using  Sobel filter\n");   
  // #pragma omp parallel for schedule(dynamic) private(i,iY,iX,Gv,Gh,G) shared(iyMax,ixMax, ER2)
  for(iY=1;iY<iyMax-1;++iY){ 
    for(iX=1;iX<ixMax-1;++iX){ 
      Gv= A[Give_i(iX-1,iY+1)] + 2*A[Give_i(iX,iY+1)] + A[Give_i(iX-1,iY+1)] - A[Give_i(iX-1,iY-1)] - 2*A[Give_i(iX-1,iY)] - A[Give_i(iX+1,iY-1)];
      Gh= A[Give_i(iX+1,iY+1)] + 2*A[Give_i(iX+1,iY)] + A[Give_i(iX-1,iY-1)] - A[Give_i(iX+1,iY-1)] - 2*A[Give_i(iX-1,iY)] - A[Give_i(iX-1,iY-1)];
      G = sqrt(Gh*Gh + Gv*Gv);
      i= Give_i(iX,iY); /* compute index of 1D array from indices of 2D array */
      if (G==0) {B[i]=255;} /* background */
      else {B[i]=0;}  /* boundary */
    }
  }
 
   
 
  return 0;
}

int CopyBoundariesFromA2B(unsigned char A[], unsigned char B[])
{
 
  unsigned int iX,iY; /* indices of 2D virtual array (image) = integer coordinate */
  unsigned int i; /* index of 1D array  */
 
 
  printf("copy boundaries from edge array to data array \n");
  for(iY=1;iY<iyMax-1;++iY)
    for(iX=1;iX<ixMax-1;++iX)
      {i= Give_i(iX,iY); if (A[i]==0) B[i]=0;}
 
 
 
  return 0;
}



 
 


 

// save "A" array to pgm file 
int SaveArray2PGMFile( unsigned char A[], double k)
{
  
  FILE * fp;
  const unsigned int MaxColorComponentValue=255; /* color component is coded from 0 to 255 ;  it is 8 bit color file */
  char name [30]; /* name of file */
  sprintf(name,"f%.0f", k); /*  */
  char *filename =strcat(name,".pgm");
  char *comment="# Numerical approximation of  Julia set for f(z)= z^2 after plane transformation; Adam Majewski";/* comment should start with # */

  /* save image to the pgm file  */      
  fp= fopen(filename,"wb"); /*create new file,give it a name and open it in binary mode  */
  fprintf(fp,"P5\n %s\n %u %u\n %u\n",comment,iWidth,iHeight,MaxColorComponentValue);  /*write header to the file*/
  fwrite(A,iSize,1,fp);  /*write A array to the file in one step */
  printf("File %s saved. \n", filename);
  fclose(fp);

  return 0;
}

int info()
{
  // diplay info messages
  printf("Numerical approximation of   \n");
  printf("Image Width = %f \n", ZxMax-ZxMin);
  printf("PixelWidth = %f \n", PixelWidth);
  printf("Maximal number of iterations = iterMax = %ld \n", iterMax);
  printf("ratio of image  = %f ; it should be 1.000 ...\n", ratio);
  printf("Unknown pixels = %ld ; it should be 0 ...\n", iUknownPixels);
  return 0;
}

/* -----------------------------------------  main   -------------------------------------------------------------*/
int main()
{

 
  setup();
 

 
   
 
  FillArray(data, iterMax, 1 ); // level set method
  SaveArray2PGMFile( data, iHeight+0); // save array data (components of Fatou set ) to pgm file

  ComputeBoundariesFromA2B(data, edge1);
  SaveArray2PGMFile( edge1, iHeight+1); // save array edge (Julia set ) to pgm file

  FillArray(data, iterMax, 2 ); // binary decomposition method
  SaveArray2PGMFile( data, iHeight+2); // save array data (components of Fatou set ) to pgm file

  ComputeBoundariesFromA2B(data, edge);
  SaveArray2PGMFile( edge, iHeight+3); // save array edge (Julia set ) to pgm file

  

  CopyBoundariesFromA2B(edge1, edge); // boundary = boundary from LSM + boundary from BDM
  SaveArray2PGMFile( edge, iHeight+4); // save array data (Julia set and components ) to pgm file
  CopyBoundariesFromA2B(edge, data);
  SaveArray2PGMFile( data, iHeight+5); // save array data (Julia set and components ) to pgm file

  

  
  

  

  printf(" allways free memory  to avoid buffer overflow \n");
  free(data);
  free(edge);
  free(edge1);
 

  
  info();

 

  return 0;
}
